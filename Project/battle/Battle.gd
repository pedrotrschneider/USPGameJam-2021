class_name Battle
extends Spatial

signal battle_over();
signal turn_finished();

const GUILD_DATA_PATH : String = "res://resources/runtime_data/GuildData.tres";
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";

export(NodePath) onready var turn_queue = self.get_node(turn_queue) as TurnQueue;
export(NodePath) onready var camera = self.get_node(camera) as Camera;
export var battle_type : int = 1;

var allies_won : bool = true;


func _ready() -> void:
	camera.current = true;
	if battle_type == 1:
		var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
		var i : int = 0;
		for g in guild_data.current_party:
			turn_queue.get_child(i).add_child(BattleManager.GUILD_FIGHTER_SCENES[g].instance());
			i += 1;
	turn_queue.initialize();
	play_turn();


func play_turn() -> void:
	check_win();
	GameEvents.emit_battle_new_turn();
	var active_fighter : Fighter = turn_queue.get_active_fighter();
	BattleState.active_fighter = active_fighter;
	
	if not active_fighter.dead:
		active_fighter.hide_target_indicator();
		active_fighter.show_active_indicator();
		
		active_fighter.choose_action_type();
		yield(active_fighter, "action_chosen");
		var action_type : int = active_fighter.get_action_type();
#		print(action_type);
		
		match action_type:
			FightConstants.ActionTypes.ATTACK:
				active_fighter.choose_target();
				yield(active_fighter, "target_chosen");
				var target_index : int = active_fighter.get_target();
#				print(target_index);
				var target_fighter : Fighter = turn_queue.get_child(target_index).get_fighter();
				var damage : int = calculate_damage(active_fighter, target_fighter);
				target_fighter.take_damage(damage);
#				print("Attack");
			
			FightConstants.ActionTypes.DEFEND:
#				print("Defend");
				pass;
			
			FightConstants.ActionTypes.DO_NOTHING:
#				print("Do nothing");
				pass;
			
			_:
				print("Invalid action type");
		
		BattleState.state = BattleState.Idle;
		yield(self.get_tree().create_timer(1.0), "timeout");
	
	active_fighter.hide_active_indicator();
	self.emit_signal("turn_finished");
	turn_queue.play_turn();
	play_turn();


func calculate_damage(attacker : Fighter, _target : Fighter) -> int:
	return attacker.attack;


func check_win() -> void:
	var enemies_alive : int = 0;
	var allies_alive : int = 0;
	for fp in turn_queue.get_children():
		if fp is FighterPosition:
			if fp.ally:
				allies_alive += 0 if fp.get_fighter().dead else 1;
			else:
				enemies_alive += 0 if fp.get_fighter().dead else 1;
	if enemies_alive == 0:
		allies_won = true;
		self.emit_signal("battle_over");
		if battle_type == 2:
			var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
			player_data.space_ships_killed += 2;
			Save.save_player_runtime_data(player_data);
	elif allies_alive == 0:
		allies_won = false;
		self.emit_signal("battle_over");
