extends Node

signal battle_target_selected(index);
func emit_battle_target_selected(index : int) -> void:
	self.emit_signal("battle_target_selected", index);


signal battle_new_turn();
func emit_battle_new_turn() -> void:
	self.emit_signal("battle_new_turn");


signal player_interacted();
func emit_player_interacted() -> void:
	self.emit_signal("player_interacted");


signal pause_player();
func emit_pause_player() -> void:
	self.emit_signal("pause_player");


signal unpause_player();
func emit_unpause_player() -> void:
	self.emit_signal("unpause_player");
