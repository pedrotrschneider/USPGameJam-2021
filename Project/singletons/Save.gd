extends Node

const GUILD_DATA_DEFAULT_PATH = "res://resources/default_data/GuildDataDefault.tres";
const LUMEN_CUSTOMIZATION_DATA_DEFAULT_PATH = "res://resources/default_data/LumenCustomizationDataDefualt.tres";
const NPC_DATA_DEFAULT_PATH = "res://resources/default_data/NPCDataDefault.tres";
const PLAYER_DATA_DEFAULT_PATH = "res://resources/default_data/PlayerDataDefault.tres";
const QUESTS_DATA_DEFAULT_PATH = "res://resources/default_data/QuestsDataDefault.tres";

const LUMEN_CUSTOMIZATION_RUNTIME_DATA_PATH = "res://resources/runtime_data/LumenCustomizationData.tres";
const GUILD_RUNTIME_DATA_PATH = "res://resources/runtime_data/GuildData.tres";
const NPC_RUNTIME_DATA_PATH = "res://resources/runtime_data/NPCData.tres";
const PLAYER_RUNTIME_DATA_PATH = "res://resources/runtime_data/PlayerData.tres";

const LUMEN_CUSTOMIZATION_SAVED_DATA_PATH = "res://resources/saved_data/LumenCustomizationData.tres";
const GUILD_SAVED_DATA_PATH = "res://resources/saved_data/GuildData.tres";
const QUESTS_SAVED_DATA_PATH = "res://resources/saved_data/QuestsData.tres";

func save_lumen_customization_runtime_data(lcd : LumenCustomizationData) -> void:
	assert(ResourceSaver.save(LUMEN_CUSTOMIZATION_RUNTIME_DATA_PATH, lcd) == OK);


func save_guild_runtime_data(gd : GuildData) -> void:
	assert(ResourceSaver.save(GUILD_RUNTIME_DATA_PATH, gd) == OK);


func save_npc_runtime_data(npcd : NPCData) -> void:
	assert(ResourceSaver.save(NPC_RUNTIME_DATA_PATH, npcd) == OK);


func save_player_runtime_data(prd : PlayerData) -> void:
	assert(ResourceSaver.save(PLAYER_RUNTIME_DATA_PATH, prd) == OK);


func save_quests_data(qd : QuestsData) -> void:
	assert(ResourceSaver.save(QUESTS_SAVED_DATA_PATH, qd) == OK);


func new_game() -> void:
	var default_guild_data = load(GUILD_DATA_DEFAULT_PATH);
	var default_lcd = load(LUMEN_CUSTOMIZATION_DATA_DEFAULT_PATH);
	var default_npc_data = load(NPC_DATA_DEFAULT_PATH);
	var default_player_data = load(PLAYER_DATA_DEFAULT_PATH);
	var default_quests_data = load(QUESTS_DATA_DEFAULT_PATH);
	
	save_guild_runtime_data(default_guild_data);
	save_lumen_customization_runtime_data(default_lcd);
	save_npc_runtime_data(default_npc_data);
	save_player_runtime_data(default_player_data);
	save_quests_data(default_quests_data);
