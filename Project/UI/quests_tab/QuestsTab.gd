extends CenterContainer

export(PackedScene) var QUEST_ITEM_SCENE;
export(NodePath) onready var available_quest_item_container = self.get_node(available_quest_item_container) as Control;
export(NodePath) onready var active_quest_item_container = self.get_node(active_quest_item_container) as Control;
export(NodePath) onready var completed_quest_item_container = self.get_node(completed_quest_item_container) as Control;


func _ready() -> void:
	var available_quests : Array = QuestsManager.get_available();
	for q in available_quests:
		var qi = QUEST_ITEM_SCENE.instance();
		qi.quest = q;
		available_quest_item_container.add_child(qi);
	
	var active_quests : Array = QuestsManager.get_active();
	for q in active_quests:
		var qi = QUEST_ITEM_SCENE.instance();
		qi.quest = q;
		active_quest_item_container.add_child(qi);
	
	var completed_quests : Array = QuestsManager.get_completed();
	for q in completed_quests:
		var qi = QUEST_ITEM_SCENE.instance();
		qi.quest = q;
		completed_quest_item_container.add_child(qi);
