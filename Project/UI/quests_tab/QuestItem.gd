extends VBoxContainer

export(NodePath) onready var name_button = self.get_node(name_button) as Button;
export(NodePath) onready var description_label = self.get_node(description_label) as Label;
export(NodePath) onready var location_label = self.get_node(location_label) as Label;
export(NodePath) onready var info_container = self.get_node(info_container) as Control;

var quest : Quest = null;


func _ready() -> void:
	name_button.connect("pressed", self, "_on_name_button_pressed");
	
	name_button.text = quest.quest_name;
	description_label.text = quest.quest_description;
	location_label.text = "Local: " + Planets.planet_names[quest.location];


func _on_name_button_pressed() -> void:
	info_container.visible = !info_container.visible;
