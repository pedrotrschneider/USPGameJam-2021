extends Control

export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;
export(NodePath) onready var new_game_button = self.get_node(new_game_button) as Button;
export(NodePath) onready var exit_button = self.get_node(exit_button) as Button;

func _ready() -> void:
	continue_button.connect("pressed", self, "_on_continue_button_pressed");
	new_game_button.connect("pressed", self, "_on_new_game_button_pressed");
	exit_button.connect("pressed", self, "_on_exit_button_pressed");


func _on_new_game_button_pressed() -> void:
	Save.new_game();
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene("res://levels/starting_hangar/StartingHangar.tscn");


func _on_exit_button_pressed() -> void:
	self.get_tree().quit();
