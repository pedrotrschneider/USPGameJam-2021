extends CenterContainer

const GUILD_DATA_PATH = "res://resources/runtime_data/GuildData.tres"

export(Array, NodePath) var captain_select_buttons_paths = [];
export(Array, NodePath) var remove_select_buttons_paths = [];
export(NodePath) onready var captain_confirm_button = self.get_node(captain_confirm_button) as Button;
export(NodePath) onready var remove_confirm_button = self.get_node(remove_confirm_button) as Button;

var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
var remove_select_buttons : Array = [];
var captain_select_buttons : Array = [];
var num_members : int = 0;
var captain_selected : bool = false;


func _ready() -> void:
	captain_confirm_button.connect("pressed", self, "_on_captain_confirm_button_pressed");
	remove_confirm_button.connect("pressed", self, "_on_remove_confirm_button_pressed");
	
	update_party_portraits();
	for csb in captain_select_buttons:
		# warning-ignore:return_value_discarded
		csb.connect("portrait_selected", self, "_on_captain_portrait_selected");
	for tsb in remove_select_buttons:
		# warning-ignore:return_value_discarded
		tsb.connect("portrait_selected", self, "_on_remove_portrait_selected");


func _on_captain_portrait_selected(index : int) -> void:
	for csb in captain_select_buttons:
		csb.captain = false;
	captain_select_buttons[index].captain = true;


func _on_remove_portrait_selected(index : int) -> void:
	if not remove_select_buttons[index].captain:
		remove_select_buttons[index].remove = not remove_select_buttons[index].remove;


func _on_captain_confirm_button_pressed() -> void:
	for i in captain_select_buttons.size():
		if captain_select_buttons[i].captain:
			guild_data.current_captain = i;
			break;
	Save.save_guild_runtime_data(guild_data);
	update_party_portraits();


func _on_remove_confirm_button_pressed() -> void:
	for i in remove_select_buttons.size():
		if remove_select_buttons[i].remove:
			if guild_data.current_party.find(i) >= 0:
				guild_data.current_party.remove(guild_data.current_party.find(i));
	Save.save_guild_runtime_data(guild_data);
	update_party_portraits();


func update_party_portraits() -> void:
	var i : int = 0;
	for csbp in captain_select_buttons_paths:
		var csb : GuildPortrait = self.get_node(csbp);
		captain_select_buttons.append(csb);
		if not guild_data.current_party.has(i):
			csb.hide();
		elif guild_data.current_captain == i:
			csb.captain = true;
		else:
			csb.captain = false;
			csb.remove = false;
		i += 1;
	
	i = 0;
	for tsbp in remove_select_buttons_paths:
		var tsb : GuildPortrait = self.get_node(tsbp);
		remove_select_buttons.append(tsb);
		if not guild_data.current_party.has(i):
			tsb.hide();
		elif guild_data.current_captain == i:
			tsb.captain = true;
		else:
			tsb.captain = false;
			tsb.remove = false;
		i += 1;
