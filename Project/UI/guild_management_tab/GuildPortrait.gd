tool
class_name GuildPortrait
extends TextureButton

signal portrait_selected(index);

export var index : int = 0;

export var captain : bool = false setget set_captain;
export var remove : bool = false setget set_remove;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	self.connect("pressed", self, "_on_pressed");


func _on_pressed() -> void:
	self.emit_signal("portrait_selected", index);


func set_captain(val : bool) -> void:
	captain = val;
	if captain:
		$ColorRect.self_modulate = Color.yellow;
	else:
		$ColorRect.self_modulate = Color.black;


func set_remove(val : bool) -> void:
	remove = val;
	if remove:
		$ColorRect.self_modulate = Color.red;
	else:
		$ColorRect.self_modulate = Color.black;
