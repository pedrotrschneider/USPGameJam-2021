extends Spatial

const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";


func _ready() -> void:
	var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
	player_data.current_planet = Planets.PlanetID.SPACE;
	Save.save_player_runtime_data(player_data);
