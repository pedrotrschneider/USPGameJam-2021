extends Spatial


func _ready() -> void:
	GameplayStates.current_planet = Planets.PlanetID.DECIMUS;
	
	for c in self.get_children():
		if c is NPC:
			c.npc_logic._on_update();
