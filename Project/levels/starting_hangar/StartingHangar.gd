extends Spatial

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var camera = self.get_node(camera) as Camera;


func _ready() -> void:
	animation_player.play("scene1");
	var dialogue_node : Node = Dialogic.start("Início/pre_Batalha");
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	BattleManager.start_battle(self, BattleManager.INITIAL_BATTLE);
	yield(BattleManager, "battle_over");
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
	animation_player.play("scene2");
	dialogue_node = Dialogic.start("Início/pos_batalha");
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	animation_player.play("scene3");
	dialogue_node = Dialogic.start("Início/hangar");
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	camera.current = false;
	dialogue_node = Dialogic.start("Início/Lumen");
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene("res://levels/internal_hangar/InternalHangar.tscn");
