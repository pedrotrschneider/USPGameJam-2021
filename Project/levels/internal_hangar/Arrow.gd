class_name UIArrow
extends Sprite3D

signal arrow_clicked();

export(NodePath) onready var area = self.get_node(area) as Area;

var mouse_over : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");


func _physics_process(_delta: float) -> void:
	if mouse_over:
		if Input.is_action_just_pressed("left_mouse_press"):
			self.emit_signal("arrow_clicked");


func _on_area_mouse_entered() -> void:
	var nm : SpatialMaterial = load("res://resources/ArrowMaterial.tres").duplicate();
	nm.albedo_color = Color.green;
	self.material_override = nm;
	mouse_over = true;


func _on_area_mouse_exited() -> void:
	var nm : SpatialMaterial = load("res://resources/ArrowMaterial.tres").duplicate();
	nm.albedo_color = Color.white;
	self.material_override = nm;
	mouse_over = false;


func enable() -> void:
	area.monitorable = true;
	area.monitoring = true;


func disable() -> void:
	area.monitorable = false;
	area.monitoring = false;
	mouse_over = false;
