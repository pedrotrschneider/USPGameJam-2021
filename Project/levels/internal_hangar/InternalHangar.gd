extends Spatial

const SECTIONS = ["L", "C", "R"];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";

export(NodePath) onready var left_button = self.get_node(left_button) as TextureButton;
export(NodePath) onready var right_button = self.get_node(right_button) as TextureButton;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var goto_planet_button = self.get_node(goto_planet_button) as Button;
export(NodePath) onready var goto_space_button = self.get_node(goto_space_button) as Button;
export(NodePath) onready var leave_dialogue = self.get_node(leave_dialogue) as Control;

var current_section : int = 1;
var goto : String;


func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
	left_button.connect("pressed", self, "_on_left_button_pressed");
	left_button.connect("mouse_entered", self, "_on_left_button_mouse_entered");
	left_button.connect("mouse_exited", self, "_on_left_button_mouse_exited");
	right_button.connect("pressed", self, "_on_right_button_pressed");
	right_button.connect("mouse_entered", self, "_on_right_button_mouse_entered");
	right_button.connect("mouse_exited", self, "_on_right_button_mouse_exited");
	
	goto_space_button.connect("pressed", self, "_on_goto_space_button_pressed");
	goto_planet_button.connect("pressed", self, "_on_goto_planet_button_pressed")
	


func _physics_process(_delta: float) -> void:
	leave_dialogue.visible = current_section == 2;
	
	left_button.visible = current_section != 0;
	if not left_button.visible:
		left_button.modulate = Color.white;
	right_button.visible = current_section != 2;
	if not right_button.visible:
		right_button.modulate = Color.white;


func _on_left_button_pressed() -> void:
	animation_player.play(SECTIONS[current_section] + SECTIONS[current_section - 1]);
	current_section -= 1;


func _on_left_button_mouse_entered() -> void:
	left_button.modulate = Color.green;


func _on_left_button_mouse_exited() -> void:
	left_button.modulate = Color.white;


func _on_right_button_pressed() -> void:
	animation_player.play(SECTIONS[current_section] + SECTIONS[current_section + 1]);
	current_section += 1;


func _on_right_button_mouse_entered() -> void:
	right_button.modulate = Color.green;


func _on_right_button_mouse_exited() -> void:
	right_button.modulate = Color.white;


func _on_goto_space_button_pressed() -> void:
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene("res://levels/Space.tscn");


func _on_goto_planet_button_pressed() -> void:
	var player_data := load(PLAYER_DATA_PATH) as PlayerData
	var next_scene : String;
	match player_data.current_planet:
		Planets.PlanetID.DECIMUS:
			next_scene = "res://levels/decimus/DecimusLand.tscn";
		Planets.PlanetID.QUATTUOR:
			next_scene = "res://levels/quattuor/QuattuorLand.tscn";
		Planets.PlanetID.VIGINITI:
			next_scene = "res://levels/viginitti/ViginittiLand.tscn";
		Planets.PlanetID.NONA:
			next_scene = "res://levels/nona/NonaLand.tscn"
		Planets.PlanetID.SPACE:
			next_scene = "res://levels/Space.tscn";
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene(next_scene);
