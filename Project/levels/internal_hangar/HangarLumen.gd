extends Spatial

const LUMEN_CUSTOMIZATION_DATA_PATH = "res://resources/runtime_data/LumenCustomizationData.tres";
const LUMEN_DATA_FIELDS = [
	"current_top_weapon",
	"current_side_weapon_front",
	"current_side_weapon_mid",
	"current_side_weapon_back",
	"current_propulsor"
];

export(Array, NodePath) var visual_paths = [];
export(Array, NodePath) var selectors_paths = [];

var lumen_customization_data : LumenCustomizationData = load(LUMEN_CUSTOMIZATION_DATA_PATH) as LumenCustomizationData;
var rotation_speed : float = 1;
var visuals : Array = [];
var selectors : Array = []


func _ready() -> void:
	for i in visual_paths.size():
		var v : Spatial = self.get_node(visual_paths[i]);
		visuals.append(v);
		v.get_child(lumen_customization_data[LUMEN_DATA_FIELDS[i]]).show();
	
	for sp in selectors_paths:
		var s : LumenSelector = self.get_node(sp);
		# warning-ignore:return_value_discarded
		s.connect("selector_pressed", self, "_on_selector_pressed");
		# warning-ignore:return_value_discarded
		s.connect("selector_released", self, "_on_selector_released");
		# warning-ignore:return_value_discarded
		s.connect("arrow_pressed", self, "_on_selector_arrow_pressed");
		selectors.append(s);


func _physics_process(delta: float) -> void:
	var rot : float = Input.get_action_strength("hangar_rotate_right") \
		- Input.get_action_strength("hangar_rotate_left");
	self.rotation.y += rot * rotation_speed * delta;


func _on_selector_pressed() -> void:
	for s in selectors:
		s.disable();


func _on_selector_released() -> void:
	for s in selectors:
		s.enable();


func _on_selector_arrow_pressed(selector_type : int, val : int) -> void:
	var v : Spatial = visuals[selector_type];
	var current : int = 0;
	for c in v.get_children():
		if c.visible:
			current = c.get_index();
	current += val;
	if current < 0 || current >= v.get_child_count(): return;
	for c in v.get_children():
		c.hide();
	v.get_child(current).show();
	lumen_customization_data[LUMEN_DATA_FIELDS[selector_type]] = current;
	Save.save_lumen_customization_runtime_data(lumen_customization_data);
