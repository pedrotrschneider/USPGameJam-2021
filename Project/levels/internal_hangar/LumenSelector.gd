class_name LumenSelector
extends Spatial

enum SelectorType {
	TOP,
	SIDE_FRONT,
	SIDE_MID,
	SIDE_BACK,
	PROPULSOR
}

signal selector_pressed();
signal selector_released();
signal arrow_pressed(selector_type, value);

export(NodePath) onready var area = self.get_node(area) as Area;
export(NodePath) onready var up_arrow = self.get_node(up_arrow) as UIArrow;
export(NodePath) onready var down_arrow = self.get_node(down_arrow) as UIArrow;
export(SelectorType) var selector_type = 0;

var mouse_over : bool = false;
var pressed : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	up_arrow.connect("arrow_clicked", self, "_on_up_arrow_clicked");
	down_arrow.connect("arrow_clicked", self, "_on_down_arrow_clicked");


func _physics_process(_delta: float) -> void:
	if Input.is_action_just_pressed("left_mouse_press"):
		if mouse_over:
			pressed = true;
			self.emit_signal("selector_pressed");
			area.transform.origin = Vector3(1000, 1000, 100);
			up_arrow.show();
			down_arrow.show();
		elif not (up_arrow.mouse_over or down_arrow.mouse_over):
			if pressed:
				pressed = false;
				self.emit_signal("selector_released");
				area.transform.origin = Vector3.ZERO;
				up_arrow.hide();
				down_arrow.hide();


func _on_area_mouse_entered() -> void:
	mouse_over = true;


func _on_area_mouse_exited() -> void:
	mouse_over = false;


func _on_up_arrow_clicked() -> void:
	self.emit_signal("arrow_pressed", selector_type, 1);


func _on_down_arrow_clicked() -> void:
	self.emit_signal("arrow_pressed", selector_type, -1);


func enable() -> void:
	area.transform.origin = Vector3.ZERO;
	area.monitoring = true;
	area.monitorable = true;
	up_arrow.enable();
	down_arrow.enable();


func disable() -> void:
	area.transform.origin = Vector3(1000, 1000, 1000);
	area.monitorable = false;
	area.monitoring = false;
	up_arrow.disable();
	down_arrow.disable();
	mouse_over = false;
