class_name PlanetLevel
extends Spatial

const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";

export(Planets.PlanetID) var planet_index : int = 0;

func _ready() -> void:
	var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
	player_data.current_planet = planet_index;
	Save.save_player_runtime_data(player_data);
	update_npcs();


func update_npcs() -> void:
	for c in self.get_children():
		if c is NPC:
			c.npc_logic._on_update();
