extends Node

enum PlanetID{
	SPACE,
	GARAGE,
	DECIMUS,
	NONA,
	QUATTUOR,
	VIGINITI
};

var planet_names : Array = [
	"Espaço",
	"Garagem",
	"Decimus",
	"Nova",
	"Quator",
	"Viginiti"
];
