extends Node

enum ActionTypes {
	ATTACK,
	DEFEND,
	DO_NOTHING,
	ESCAPE
}
