class_name LumenCustomizationData
extends Resource

export var current_top_weapon : int = 0;
export var current_side_weapon_front : int = 0;
export var current_side_weapon_mid : int = 0;
export var current_side_weapon_back : int = 0;
export var current_propulsor : int = 0;
