class_name PlayerData
extends Resource

export(Planets.PlanetID) var current_planet : int = 0;
export var player_money : int = 0;
export var space_ships_killed : int = 0;
export var ores_collected : int = 0;
