class_name NPCData
extends Resource

# Decimus
export var gruvi_state : int = 0;
export var wufra_state : int = 0;
export var zhalan_state : int = 0;

# Nona
export var pleotheo_state : int = 0;
export var zimu_state : int = 0;
export var inimigo_yamo_state : int = 0;
export var yamo_state : int = 0;

# Viginiti
export var kiva_state : int = 0;
export var estra_state : int = 0;
export var lisi_state : int = 0;

# Quattuor
export var skypelt_state : int = 0;
export var koknif_state : int = 0;
export var veza_state : int = 0;
