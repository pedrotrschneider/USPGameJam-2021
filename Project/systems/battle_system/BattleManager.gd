extends Node

signal battle_over();

enum {
	TEST_BATTLE,
	INITIAL_BATTLE,
	YAMO_BATTLE,
	ESTRA_BATTLE,
	SPACE_BATTLE_1,
	SPACE_BATTLE_2,
	SPACE_BATTLE_3,
	FINAL_BATTLE
}

export(Array, PackedScene) var BATTLE_SCENES = [];
export(Array, PackedScene) var GUILD_FIGHTER_SCENES = [];

func start_battle(caller : Node, battle_index : int) -> void:
	caller.hide();
	caller.pause_mode = Node.PAUSE_MODE_STOP;
	self.get_tree().paused = true;
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
	
	var battle : Battle = BATTLE_SCENES[battle_index].instance() as Battle;
	self.add_child(battle);
	yield(battle, "battle_over");
	if battle.allies_won:
		battle.queue_free();
	else:
		print("allies lost");
	
	caller.pause_mode = Node.PAUSE_MODE_PROCESS;
	self.get_tree().paused = false;
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
	caller.show();
	if caller is PlanetLevel:
		caller.update_npcs();
	self.emit_signal("battle_over");
