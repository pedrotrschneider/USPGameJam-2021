extends Node

const QUESTS_SAVED_DATA_PATH : String = "res://resources/saved_data/QuestsData.tres";

export(PackedScene) var quests_display_scene;
export(NodePath) onready var quests = self.get_node(quests) as Node;
export(NodePath) onready var available = self.get_node(available) as Node;
export(NodePath) onready var active = self.get_node(active) as Node;
export(NodePath) onready var completed = self.get_node(completed) as Node;
export(NodePath) onready var failed = self.get_node(failed) as Node;


func _ready() -> void:
	var quests_data : QuestsData = load(QUESTS_SAVED_DATA_PATH) as QuestsData;
	for q in quests.get_children():
		if q is Quest:
			quests.remove_child(q);
			match quests_data.quests_status[q.quest_id]:
				0:
					available.add_child(q);
				1:
					active.add_child(q);
				2:
					completed.add_child(q);
				3:
					failed.add_child(q);


func _physics_process(_delta: float) -> void:
	if Input.is_action_just_pressed("toggle_missions"):
		if self.get_child_count() == 5:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
			var qt = quests_display_scene.instance();
			self.add_child(qt);
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
			self.get_child(5).queue_free();


func activate_quest(id : Array) -> void:
	for q in available.get_children():
		if q is Quest:
			if q.quest_id == int(id[0]):
				available.remove_child(q);
				active.add_child(q);


func complete_quest(id : Array) -> void:
	for q in active.get_children():
		if q is Quest:
			if q.quest_id == int(id[0]):
				active.remove_child(q);
				completed.add_child(q);


func fail_quest(id : Array) -> void:
	for q in active.get_children():
		if q is Quest:
			if q.quest_id == int(id[0]):
				active.remove_child(q);
				failed.add_child(q);


func is_quest_available(id : int) -> bool:
	for q in available.get_children():
		if q is Quest:
			if q.quest_id == id:
				return true;
	return false;


func is_quest_active(id : int) -> bool:
	for q in active.get_children():
		if q is Quest:
			if q.quest_id == id:
				return true;
	return false;


func is_quest_completed(id : int) -> bool:
	for q in completed.get_children():
		if q is Quest:
			if q.quest_id == id:
				return true;
	return false;


func get_available() -> Array:
	return available.get_children();


func get_available_count() -> int:
	return available.get_child_count();


func get_active() -> Array:
	return active.get_children();


func get_active_count() -> int:
	return active.get_child_count();


func get_completed() -> Array:
	return completed.get_children();


func get_completed_count() -> int:
	return completed.get_child_count();
