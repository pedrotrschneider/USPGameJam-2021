tool
class_name PlayerCamera
extends Spatial

export var cam_height : float = 1.6 setget set_cam_height;
export var cam_orbit_radius : float = 2.2 setget set_cam_orbit_radius;
export var cam_view_angle : float = -8.0 setget set_cam_view_angle;
export var cam_position_offset : Vector2 = Vector2.ZERO setget set_cam_position_offset;
export var horizontal_sensitivity : float = 0.1;
export var vertical_sensitivity : float = 0.1;
export var horizontal_acceleration : float = 10;
export var vertical_acceleration : float = 10;
export var cam_vertical_min : float = -55.0;
export var cam_vertical_max : float = 75.0;

var cam_rotation_horizontal : float = 0.0;
var cam_rotation_vertical : float = 0.0;


func _ready() -> void:
	if not Engine.editor_hint:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
	
	$Horizozntal/Vertical/ClippedCamera.add_exception(self.get_parent());


func _input(event: InputEvent) -> void:
	if Engine.editor_hint: return;

	if event is InputEventMouseMotion:
		cam_rotation_horizontal -= event.relative.x * horizontal_sensitivity;
		cam_rotation_vertical -= event.relative.y * vertical_sensitivity;


func _physics_process(delta: float) -> void:
	if Engine.editor_hint: return;
	
	cam_rotation_vertical = clamp(cam_rotation_vertical, cam_vertical_min,\
		cam_vertical_max);
	
	$Horizozntal.rotation_degrees.y = lerp($Horizozntal.rotation_degrees.y,\
		cam_rotation_horizontal, horizontal_acceleration * delta);
	$Horizozntal/Vertical.rotation_degrees.x = \
		lerp($Horizozntal/Vertical.rotation_degrees.x, cam_rotation_vertical,\
		vertical_acceleration * delta);


func set_cam_height(val : float) -> void:
	cam_height = val;
	$Horizozntal/Vertical.transform.origin.y = val;


func set_cam_orbit_radius(val : float) -> void:
	cam_orbit_radius = val;
	$Horizozntal/Vertical/ClippedCamera.transform.origin.z = val;


func set_cam_view_angle(val : float) -> void:
	cam_view_angle = val;
	if not self.is_inside_tree(): return;
	$Horizozntal/Vertical/ClippedCamera.rotation_degrees.x = val;


func set_cam_position_offset(val : Vector2) -> void:
	cam_position_offset = val;
	$Horizozntal/Vertical/ClippedCamera.transform.origin.x = val.x;
	$Horizozntal/Vertical/ClippedCamera.transform.origin.y = val.y;


func get_horizontal() -> Spatial:
	return $Horizozntal as Spatial;


func get_vertical() -> Spatial:
	return $Horizozntal/Vertical as Spatial;


func get_camera() -> ClippedCamera:
	return $Horizozntal/Vertical/ClippedCamera as ClippedCamera;
