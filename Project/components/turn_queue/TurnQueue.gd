class_name TurnQueue
extends Node

var active_fighter_position : FighterPosition = null;


func initialize() -> void:
	for fp in self.get_children():
		fp.initialize();
		if fp.get_child_count() == 0:
			self.remove_child(fp);
			fp.queue_free();
	var fighters : Array = self.get_children();
	fighters.sort_custom(self, "sort_fighters");
	for fighter in fighters:
		fighter.raise();
	active_fighter_position = self.get_child(0);


static func sort_fighters(a : FighterPosition, b : FighterPosition) -> bool:
	return a.get_fighter().speed > b.get_fighter().speed;


func play_turn() -> void:
	var new_index : int = (active_fighter_position.get_index() + 1) % self.get_child_count();
	active_fighter_position = self.get_child(new_index);


func get_active_fighter() -> Fighter:
	return active_fighter_position.get_fighter();


func get_allies() -> Array:
	var allies : Array = [];
	for i in 4:
		var fp : FighterPosition = self.get_child(i + 4);
		if fp.get_fighter(): allies.append(fp);
	return allies;


func get_enemies() -> Array:
	var enemies : Array = [];
	for i in 4:
		var fp : FighterPosition = self.get_child(i + 4);
		if fp.get_fighter(): enemies.append(fp);
	return enemies;
