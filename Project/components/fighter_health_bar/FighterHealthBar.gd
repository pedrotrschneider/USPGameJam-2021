extends Sprite3D

var fighter : Fighter = null;
var max_health : float = 0;

func _ready() -> void:
	fighter = self.get_parent() as Fighter;
	max_health = fighter.hp;


func _physics_process(_delta: float) -> void:
	self.scale.x = fighter.hp / max_health;
