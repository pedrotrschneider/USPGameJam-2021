class_name NPCLogic
extends Node

signal interaction_finished();

const NPC_DATA_PATH : String = "res://resources/runtime_data/NPCData.tres";


func _on_update() -> void:
	pass


func _on_interact() -> void:
	self.emit_signal("interaction_finished");
