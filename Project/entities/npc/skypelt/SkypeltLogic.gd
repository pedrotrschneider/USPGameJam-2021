class_name SkypeltLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Quattuor/Skypelt/tl1",
	"Quattuor/Skypelt/pos_tl1",
	"Quattuor/Skypelt/opções",
	"Quattuor/Skypelt/pendente",
	"Quattuor/Skypelt/pos_missao",
	"Quattuor/Skypelt/fim",
];


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.koknif_state >= 3:
		self.get_parent().queue_free();
	elif npc_data.skypelt_state == 2:
		npc_data.skypelt_state += 1;
	
	Save.save_npc_runtime_data(npc_data);


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.skypelt_state]);
	
	if npc_data.skypelt_state == 0:
		npc_data.skypelt_state += 1;
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
	elif npc_data.skypelt_state == 1:
		if npc_data.veza_state < 1 and npc_data.koknif_state == 1:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			if Dialogic.get_variable("answer") == "0":
				npc_data.skypelt_state += 1;
		else:
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
	elif npc_data.skypelt_state == 2:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
	elif npc_data.skypelt_state == 3:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
		npc_data.skypelt_state += 1;
	elif npc_data.skypelt_state == 4:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[5]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
	
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
