class_name GruviLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Decimus/Gruvi/pre_missao1",
	"Decimus/Gruvi/pre_missao2",
	"Decimus/Gruvi/pendente",
	"Decimus/Gruvi/pos_missao",
	"Decimus/Gruvi/fim"
];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";
const SPACE_TROOPS_MISSION_INDEX : int = 5;

var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;


func _on_interact() -> void:
	npc_data = load(NPC_DATA_PATH) as NPCData;
	player_data = load(PLAYER_DATA_PATH) as PlayerData;
	var dialogue : String = DIALOGUE_NAMES[npc_data.gruvi_state];
	match npc_data.gruvi_state:
		0:
			npc_data.gruvi_state += 1;
		1:
			npc_data.gruvi_state += 1; 
		2:
			if player_data.space_ships_killed >= 20:
				npc_data.gruvi_state += 1;
				dialogue = DIALOGUE_NAMES[npc_data.gruvi_state];
				npc_data.gruvi_state += 1;
				npc_data.inimigo_yamo_state += 1;
	Save.save_npc_runtime_data(npc_data);
	
	var dialogue_node : Node = Dialogic.start(dialogue)
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	self.emit_signal("interaction_finished");
