class_name EstraLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Viginiti/Estra/pre_missao",
	"Viginiti/Estra/pendente",
	"Viginiti/Estra/pos_missao",
	"Viginiti/Estra/fim",
	"Viginiti/Estra/opções",
	"Viginiti/Estra/pos_batalha",
];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node;
	
	if npc_data.estra_state == 0:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[0]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
		npc_data.estra_state += 1;
	elif npc_data.estra_state == 1 or npc_data.estra_state == 3:
		if npc_data.kiva_state == 1:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			var answer : int = int(Dialogic.get_variable("answer"));
			if answer == 0:
				BattleManager.start_battle(self.get_parent().get_parent(), BattleManager.ESTRA_BATTLE);
				yield(BattleManager, "battle_over");
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[5]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
				npc_data.kiva_state = 2;
			elif answer == 1:
				npc_data.kiva_state = 3;
		else:
			var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
			if npc_data.estra_state == 1 and \
			   player_data.ores_collected == 3:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
				npc_data.estra_state = 3;
			else:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[npc_data.estra_state]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
	
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
