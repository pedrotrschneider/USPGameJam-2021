class_name PleotheoLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Nona/Pleotheo/pre_missao",
	"Nona/Pleotheo/pendente",
	"Nona/Pleotheo/pos_missao",
	"Nona/Pleotheo/pos_missao_fracasso",
	"Nona/Pleotheo/fim",
];


func _on_update() -> void:
	pass


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.pleotheo_state]);
	
	match npc_data.pleotheo_state:
		0:
			npc_data.pleotheo_state += 1;
		1:
			if npc_data.zimu_state == 2:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
				npc_data.pleotheo_state += 1;
			elif npc_data.zimu_state == 3:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
				npc_data.pleotheo_state += 1;
		2:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");

