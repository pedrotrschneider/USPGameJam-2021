class_name WufraLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Decimus/Wufra/tl1",
	"Decimus/Wufra/opções",
	"Decimus/Wufra/loja",
	"Decimus/Wufra/loja_desconto",
	"Decimus/Wufra/compra_recusada",
	"Decimus/Wufra/compra_aceita",
];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
	var dialogue_node : Node = null;
	match npc_data.wufra_state:
		0:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[0]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			npc_data.wufra_state += 1;
			Save.save_npc_runtime_data(npc_data);
			_on_interact();
		1:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[1]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			if Dialogic.get_variable("answer") == "0":
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[2 if npc_data.gruvi_state < 4 else 3]);
				var price = 100 if npc_data.gruvi_state < 4 else 50;
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
				if player_data.player_money < price:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
				else:
					player_data.player_money -= price;
					Save.save_player_runtime_data(player_data);
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[5]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
	self.emit_signal("interaction_finished");
