class_name ZimuLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Nona/Zimu/tl1",
	"Nona/Zimu/opções",
	"Nona/Zimu/pleotheo",
	"Nona/Zimu/compra_aceita",
	"Nona/Zimu/compra_rejeitada",
];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";


func _on_update() -> void:
	pass


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node;
	match npc_data.zimu_state:
		0:
			npc_data.zimu_state += 1;
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[0])
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
		1:
			Dialogic.set_variable("condition1", 1 if npc_data.pleotheo_state == 1 and npc_data.zimu_state == 1 else 0);
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[1]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			if Dialogic.get_variable("answer") == "0":
				var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
				if player_data.player_money >= 100:
					player_data.player_money -= 100;
					Save.save_player_runtime_data(player_data);
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
				else:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
			elif Dialogic.get_variable("answer") == "1":
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
				if Dialogic.get_variable("answer") == "0":
					npc_data.zimu_state = 2;
				else:
					npc_data.zimu_state = 3;
		_:
			Dialogic.set_variable("condition1", 0);
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[1]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			if Dialogic.get_variable("answer") == "0":
				var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
				if player_data.player_money >= 100:
					player_data.player_money -= 100;
					Save.save_player_runtime_data(player_data);
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
				else:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
