class_name KoknifLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Quattuor/Koknif/pre_missao",
	"Quattuor/Koknif/pendente",
	"Quattuor/Koknif/pos_missao",
	"Quattuor/Koknif/pendente2",
	"Quattuor/Koknif/pos_missao2",
	"Quattuor/Koknif/fim",
];


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.koknif_state == 3:
		npc_data.koknif_state += 1;
	elif npc_data.skypelt_state >= 2:
		self.get_parent().queue_free();
	
	Save.save_npc_runtime_data(npc_data);


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.koknif_state]);
	
	if npc_data.koknif_state == 0:
		npc_data.koknif_state += 1;
	elif npc_data.koknif_state == 1:
		if npc_data.veza_state == 1:
			npc_data.koknif_state += 1;
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[npc_data.koknif_state]);
			npc_data.koknif_state += 1;
	elif npc_data.koknif_state == 4:
		npc_data.koknif_state += 1;
	
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
