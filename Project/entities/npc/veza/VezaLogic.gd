class_name VezaLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Quattuor/Veza/constante",
	"Quattuor/Veza/missao",
	"Quattuor/Veza/pos_missao",
	"Quattuor/Veza/opções",
	"Quattuor/Veza/seguir",
	"Quattuor/Veza/nao_seguir"
];
const GUILD_DATA_PATH : String = "res://resources/runtime_data/GuildData.tres";


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.veza_state == 4:
		self.get_parent().queue_free();
		return;
	
	if npc_data.koknif_state >= 3 and npc_data.veza_state <= 2:
		npc_data.veza_state = int(min(2, npc_data.veza_state + 1));
	
	Save.save_npc_runtime_data(npc_data);


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.veza_state]);
	
	if npc_data.veza_state == 0 and \
	   npc_data.koknif_state == 1 and \
	   npc_data.skypelt_state < 2:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[1]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
		if Dialogic.get_variable("answer") == "2":
			npc_data.veza_state += 1;
	elif npc_data.veza_state == 1:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[0]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
	elif npc_data.veza_state == 2 or npc_data.veza_state == 3:
		dialogue_node = Dialogic.start(DIALOGUE_NAMES[npc_data.veza_state]);
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
		npc_data.veza_state = int(min(3, npc_data.veza_state + 1));
		if Dialogic.get_variable("answer") == "0":
			var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
			if guild_data.current_party.size() < 4:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
				npc_data.veza_state += 1;
				guild_data.current_party.append(GuildConstants.VEZA);
				Save.save_guild_runtime_data(guild_data);
			else:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[5]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
	else:
		self.add_child(dialogue_node);
		yield(dialogue_node, "tree_exited");
	
	Save.save_npc_runtime_data(npc_data);
	self.get_parent().get_parent().update_npcs();
	self.emit_signal("interaction_finished");
