class_name YamoLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Nona/Yamo/pos_batalha",
	"Nona/Yamo/opções",
	"Nona/Yamo/seguir",
	"Nona/Yamo/nao_seguir"
];
const GUILD_DATA_PATH : String = "res://resources/runtime_data/GuildData.tres";

onready var original_pos : Vector3 = self.get_parent().transform.origin;


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.yamo_state == 0 or npc_data.yamo_state == 3:
		self.get_parent().transform.origin = Vector3(1000, 1000, 1000);
	else:
		self.get_parent().transform.origin = original_pos;


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.yamo_state - 1]);
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	
	match npc_data.yamo_state:
		1:
			npc_data.yamo_state += 1;
		2:
			if Dialogic.get_variable("answer") == "0":
				var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
				if guild_data.current_party.size() < 4:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
					guild_data.current_party.append(GuildConstants.YAMO);
					npc_data.yamo_state += 1;
					Save.save_guild_runtime_data(guild_data);
					Save.save_npc_runtime_data(npc_data);
				else:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
				self.get_parent().get_parent().update_npcs();
	
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
