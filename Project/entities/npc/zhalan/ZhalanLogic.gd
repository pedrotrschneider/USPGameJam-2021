class_name ZhalanLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Decimus/Zhalan/tl1",
	"Decimus/Zhalan/opções",
	"Decimus/Zhalan/seguir",
	"Decimus/Zhalan/nao_seguir",
	"Decimus/Zhalan/sem_dinheiro",
];
const PLAYER_DATA_PATH : String = "res://resources/runtime_data/PlayerData.tres";
const GUILD_DATA_PATH : String = "res://resources/runtime_data/GuildData.tres";


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.zhalan_state == 2:
		self.get_parent().queue_free();


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node;
	dialogue_node = Dialogic.start(DIALOGUE_NAMES[npc_data.zhalan_state]);
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	match npc_data.zhalan_state:
		0:
			npc_data.zhalan_state += 1;
		1:
			if Dialogic.get_variable("answer") == "0":
				var player_data : PlayerData = load(PLAYER_DATA_PATH) as PlayerData;
				if player_data.player_money >= 500:
					var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
					if guild_data.current_party.size() < 4:
						dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
						player_data.player_money -= 500;
						guild_data.current_party.append(GuildConstants.ZHALAN);
						Save.save_guild_runtime_data(guild_data);
						Save.save_player_runtime_data(player_data);
						npc_data.zhalan_state += 1;
					else:
						dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
				else:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
