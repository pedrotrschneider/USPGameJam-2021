class_name InimigoYamoLogic
extends NPCLogic

const DIALOGUE_NAMES = [
	"Nona/Yamo/pre_batalha",
];


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.inimigo_yamo_state == 0:
		self.get_parent().queue_free();


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[0]);
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	npc_data.inimigo_yamo_state = 0;
	npc_data.yamo_state = 1;
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
	BattleManager.start_battle(self.get_parent().get_parent(), BattleManager.YAMO_BATTLE);
	yield(BattleManager, "battle_over");

