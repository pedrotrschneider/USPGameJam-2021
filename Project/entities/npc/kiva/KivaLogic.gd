class_name KivaLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Viginiti/Kiva/pre_missao",
	"Viginiti/Kiva/pendente",
	"Viginiti/Kiva/pos_missao",
	"Viginiti/Kiva/pos_missao_fracasso",
	"Viginiti/Kiva/fim",
]


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node = Dialogic.start(DIALOGUE_NAMES[npc_data.kiva_state]);
	self.add_child(dialogue_node);
	yield(dialogue_node, "tree_exited");
	
	if npc_data.kiva_state == 0:
		npc_data.kiva_state += 1;
	elif npc_data.kiva_state == 2 or \
		 npc_data.kiva_state == 3:
		npc_data.kiva_state = 4;
	
	Save.save_npc_runtime_data(npc_data);
	self.emit_signal("interaction_finished");
