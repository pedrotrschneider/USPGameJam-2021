class_name LisiLogic
extends NPCLogic

const DIALOGUE_NAMES : Array = [
	"Viginiti/Lisi/tl1",
	"Viginiti/Lisi/pendente",
	"Viginiti/Lisi/sucesso",
	"Viginiti/Lisi/opções",
	"Viginiti/Lisi/seguir",
	"Viginiti/Lisi/nao_seguir",
];
const LUMEN_CUSTOMIZATION_PATH_DATA : String = "res://resources/runtime_data/LumenCustomizationData.tres";
const GUILD_DATA_PATH : String = "res://resources/runtime_data/GuildData.tres";


func _on_update() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	if npc_data.lisi_state == 3:
		self.get_parent().queue_free();


func _on_interact() -> void:
	var npc_data : NPCData = load(NPC_DATA_PATH) as NPCData;
	var dialogue_node : Node;
	if npc_data.lisi_state == 0:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[0]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			npc_data.lisi_state += 1;
			Save.save_npc_runtime_data(npc_data);
	if npc_data.lisi_state == 1:
			var lcd : LumenCustomizationData = load(LUMEN_CUSTOMIZATION_PATH_DATA) as LumenCustomizationData;
			if lcd.current_side_weapon_front > 0 and \
			   lcd.current_side_weapon_mid > 0 and \
			   lcd.current_side_weapon_back > 0:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[2]);
				npc_data.lisi_state += 1;
			else:
				dialogue_node = Dialogic.start(DIALOGUE_NAMES[1]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
	if npc_data.lisi_state == 2:
			dialogue_node = Dialogic.start(DIALOGUE_NAMES[3]);
			self.add_child(dialogue_node);
			yield(dialogue_node, "tree_exited");
			if Dialogic.get_variable("answer") == "0":
				var guild_data : GuildData = load(GUILD_DATA_PATH) as GuildData;
				if guild_data.current_party.size() < 4:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[4]);
					guild_data.current_party.append(GuildConstants.LISI);
					Save.save_guild_runtime_data(guild_data);
					npc_data.lisi_state += 1;
				else:
					dialogue_node = Dialogic.start(DIALOGUE_NAMES[5]);
				self.add_child(dialogue_node);
				yield(dialogue_node, "tree_exited");
	
	Save.save_npc_runtime_data(npc_data);
	self.get_parent().get_parent().update_npcs();
	self.emit_signal("interaction_finished");
