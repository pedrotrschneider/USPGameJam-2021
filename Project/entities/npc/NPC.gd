class_name NPC
extends Spatial

export(NodePath) onready var area = self.get_node(area) as Area;
export(NodePath) onready var npc_logic = self.get_node(npc_logic) as NPCLogic;

var player_in_range : bool = false;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	GameEvents.connect("player_interacted", self, "_on_player_interacted");
	
	area.connect("body_entered", self, "_on_area_body_entered");
	area.connect("body_exited", self, "_on_area_body_exited");


func _on_player_interacted() -> void:
	if player_in_range:
		self.get_tree().paused = true;
		GameEvents.emit_pause_player();
		npc_logic._on_interact();
		area.transform.origin = Vector3(1000, 1000, 1000);
		yield(npc_logic, "interaction_finished");
		area.transform.origin = Vector3.ZERO;
		self.get_tree().paused = false;
		GameEvents.emit_unpause_player();


func _on_area_body_entered(body : Node) -> void:
	if body.is_in_group(Groups.player_land):
		player_in_range = true;


func _on_area_body_exited(_body : Node) -> void:
	player_in_range = false;
