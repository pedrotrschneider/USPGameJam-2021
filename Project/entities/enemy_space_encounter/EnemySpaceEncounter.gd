extends Spatial

export(Array, NodePath) onready var visuals;
export(NodePath) onready var area = self.get_node(area) as Area;

var i : int = 0;


func _ready() -> void:
	self.add_to_group(Groups.space_enemy_encounter);
	area.connect("body_entered", self, "_on_area_body_entered");
	
	randomize();
	i = randi() % 3;
	var visual : Spatial = get_node(visuals[i]) as Spatial;
	var j : int = randi() % 3;
	visual.get_child(j).show();


func _on_area_body_entered(body : Node) -> void:
	if body is Lumen:
		var battle : int = 0;
		match i:
			0:
				battle = BattleManager.SPACE_BATTLE_3;
			1:
				battle = BattleManager.SPACE_BATTLE_2;
			2:
				battle = BattleManager.SPACE_BATTLE_1;
		BattleManager.start_battle(self.get_parent(), battle);
		yield(BattleManager, "battle_over");
		self.queue_free();
