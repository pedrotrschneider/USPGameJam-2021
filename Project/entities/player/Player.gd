class_name Player
extends KinematicBody

signal travel();

const IWR_BLEND_PATH : String = "parameters/iwr/iwr_blend/blend_amount";

export(NodePath) onready var player_camera = self.get_node(player_camera) as PlayerCamera;
export(NodePath) onready var visual = self.get_node(visual) as Spatial;
export(NodePath) onready var animation_tree = self.get_node(animation_tree) as AnimationTree; 
export var gravity : float = 1.0;
export var walk_speed : float = 1.0;
export var jog_speed : float = 1.5;
export var run_speed : float = 500.0;
export var jog_threshold : float = 0.5;
export var movement_acceleration : float = 6;
export var angular_acceleration : float = 7;

var movement_speed : float = 0;
var move_direction : Vector3 = Vector3.FORWARD;
var velocity : Vector3 = Vector3.ZERO;
var fall : Vector3 = Vector3.ZERO;
var can_travel : bool = false;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	GameEvents.connect("pause_player", self, "_on_pause_player");
	# warning-ignore:return_value_discarded
	GameEvents.connect("unpause_player", self, "_on_unpause_player");
	
	self.add_to_group(Groups.player_land);
	
	visual.rotation_degrees += self.rotation_degrees;
	self.rotation_degrees = Vector3.ZERO;
	
	animation_tree.set(IWR_BLEND_PATH, -1);


func _physics_process(delta: float) -> void:
	if can_travel:
		if Input.is_action_pressed("lumen_travel"):
			self.emit_signal("travel");
	
	if Input.is_action_just_pressed("player_interact"):
		GameEvents.emit_player_interacted();
	
	if Input.is_action_pressed("player_left") or \
	   Input.is_action_pressed("player_right") or \
	   Input.is_action_pressed("player_up") or \
	   Input.is_action_pressed("player_down"):
		var horizontal_rotation : float = player_camera.get_horizontal() \
			.global_transform.basis.get_euler().y;
		
		move_direction = Vector3(Input.get_action_strength("player_right")
								 - Input.get_action_strength("player_left"),
								 0,
								 Input.get_action_strength("player_down")
								 - Input.get_action_strength("player_up"));
	
		if Input.is_action_pressed("player_sprint"):
			animation_tree.set(IWR_BLEND_PATH, lerp(animation_tree.get(IWR_BLEND_PATH), 1, delta * movement_acceleration));
			movement_speed = run_speed;
		else:
			animation_tree.set(IWR_BLEND_PATH, lerp(animation_tree.get(IWR_BLEND_PATH), 0, delta * movement_acceleration));
			if move_direction.length_squared() >= jog_threshold:
				movement_speed = jog_speed;
			else:
				movement_speed = walk_speed;
		
		move_direction = move_direction.rotated(Vector3.UP, horizontal_rotation);
		move_direction = move_direction.normalized();
	else:
		animation_tree.set(IWR_BLEND_PATH, lerp(animation_tree.get(IWR_BLEND_PATH), -1, delta * movement_acceleration));
		movement_speed = 0;
	
	velocity = lerp(velocity, move_direction * movement_speed, \
		movement_acceleration * delta);
	
	fall += Vector3.DOWN * gravity * delta;
	
	# warning-ignore:return_value_discarded
	velocity = self.move_and_slide(velocity, Vector3.UP);
	var col : KinematicCollision = self.move_and_collide(fall, true, true, true);
	if not col or col.normal.angle_to(Vector3.UP) > 1:
		self.move_and_collide(fall);
	else:
		fall = Vector3.ZERO;
	
	visual.rotation.y = lerp_angle(visual.rotation.y, atan2(-move_direction.x, \
		-move_direction.z), delta * angular_acceleration);


func _on_pause_player() -> void:
	self.pause_mode = Node.PAUSE_MODE_STOP;
	player_camera.pause_mode = Node.PAUSE_MODE_STOP;
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);


func _on_unpause_player() -> void:
	self.pause_mode = Node.PAUSE_MODE_INHERIT;
	player_camera.pause_mode = Node.PAUSE_MODE_INHERIT;
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
