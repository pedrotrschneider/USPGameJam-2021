extends Spatial


func _ready() -> void:
	var c : Array = self.get_children();
	randomize();
	c.shuffle();
	c[0].show();
