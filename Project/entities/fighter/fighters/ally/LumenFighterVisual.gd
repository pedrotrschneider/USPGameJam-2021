extends Spatial

const LUMEN_CUSTOMIZATION_DATA_PATH = "res://resources/runtime_data/LumenCustomizationData.tres";
const LUMEN_DATA_FIELDS = [
	"current_top_weapon",
	"current_side_weapon_front",
	"current_side_weapon_mid",
	"current_side_weapon_back",
	"current_propulsor"
];

export(Array, NodePath) var visual_paths = [];

var lumen_customization_data : LumenCustomizationData = load(LUMEN_CUSTOMIZATION_DATA_PATH) as LumenCustomizationData;


func _ready() -> void:
	for i in visual_paths.size():
		var v : Spatial = self.get_node(visual_paths[i]);
		v.get_child(lumen_customization_data[LUMEN_DATA_FIELDS[i]]).show();
