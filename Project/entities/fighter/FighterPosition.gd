class_name FighterPosition
extends Position3D

export var ally : bool = true;

var fighter : Fighter = null;

func initialize() -> void:
	if self.get_child_count() > 0:
		fighter = self.get_child(0) as Fighter;


func _physics_process(_delta: float) -> void:
	fighter.index = self.get_index();


func get_fighter() -> Fighter:
	return fighter;
