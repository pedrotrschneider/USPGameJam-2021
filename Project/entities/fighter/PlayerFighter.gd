class_name PlayerFighter
extends Fighter


func _on_target_selected(index : int) -> void:
	target = index;


func choose_action_type() -> void:
	BattleState.state = BattleState.PlayerChoosingAction;
	var dialogue : Node = Dialogic.start("choose_action");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	action_type = int(Dialogic.get_variable("action_type"));
	self.emit_signal("action_chosen");


func choose_target() -> void:
	BattleState.state = BattleState.PlayerChoosingTarget;
	var dialogue : Node = Dialogic.start("choose_target");
	var wr : WeakRef = weakref(dialogue);
	self.add_child(dialogue);
	if wr.get_ref() and dialogue.get_child_count() > 0:
		dialogue.get_child(0).mouse_filter = Control.MOUSE_FILTER_IGNORE;
	# warning-ignore:return_value_discarded
	GameEvents.connect("battle_target_selected", self, "_on_target_selected");
	yield(GameEvents, "battle_target_selected");
	if wr.get_ref() and dialogue.get_child_count() > 0:
		dialogue.get_child(0)._load_next_event();
	GameEvents.disconnect("battle_target_selected", self, "_on_target_selected");
	self.emit_signal("target_chosen");
