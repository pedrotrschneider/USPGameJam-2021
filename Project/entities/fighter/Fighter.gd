class_name Fighter
extends Spatial

signal action_chosen();
signal target_chosen();

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var area = self.get_node(area) as Area;
export(NodePath) onready var active_indicator = self.get_node(active_indicator) as Spatial;
export(NodePath) onready var target_indicator = self.get_node(target_indicator) as Spatial;
export var hp : int = 10;
export var attack : int = 10;
export var defense : int = 10;
export var speed : int = 10;
export var ally : bool = true setget set_ally;

var index : int = 0;
var action_type : int = 0;
var target : int = 0;
var dead : bool = false;

var mouse_over : bool = false;
var targeted : bool = false;


func _ready() -> void:
	GameEvents.connect("battle_new_turn", self, "_on_new_turn_started");
	area.connect("mouse_entered", self, "_on_mouse_entered");
	area.connect("mouse_exited", self, "_on_mouse_exited");
	
	animation_player.play("idle");


func _on_new_turn_started() -> void:
	mouse_over = false;
	targeted = false;
	hide_target_indicator();


func _physics_process(_delta: float) -> void:
	target_indicator.visible = mouse_over or targeted;
	if (BattleState.state == BattleState.PlayerChoosingTarget and \
	   BattleState.active_fighter != self and mouse_over) or targeted:
		if Input.is_action_just_pressed("left_mouse_press"):
			GameEvents.emit_battle_target_selected(index);
			targeted = true;


func _on_mouse_entered() -> void:
	mouse_over = true;


func _on_mouse_exited() -> void:
	mouse_over = false;


func choose_action_type() -> void:
	BattleState.state = BattleState.EnemyChoosingAction;
	action_type = FightConstants.ActionTypes.DO_NOTHING;
	yield(self.get_tree().create_timer(0.5), "timeout");
	self.emit_signal("action_chosen");


func choose_target() -> void:
	BattleState.state = BattleState.EnemyChoosingTarget
	target = 0;
	yield(self.get_tree().create_timer(0.5), "timeout");
	self.emit_signal("target_chosen");


func get_action_type() -> int:
	return action_type;


func get_target() -> int:
	return target;


func take_damage(damage : int) -> void:
	hp -= damage;
	dead = hp <= 0;
	if dead:
		animation_player.play("RESET");
		animation_player.play("die");
		area.queue_free();


func play_anim(name : String) -> void:
	animation_player.play("RESET");
	animation_player.play(name);


func show_active_indicator() -> void:
	active_indicator.show();


func hide_active_indicator() -> void:
	active_indicator.hide();


func show_target_indicator() -> void:
	target_indicator.show();


func hide_target_indicator() -> void:
	active_indicator.hide();


func set_ally(val : bool) -> void:
	ally = val;
	if not val:
		self.rotation_degrees.y = 0;
	else:
		self.rotation_degrees.y = 180;
