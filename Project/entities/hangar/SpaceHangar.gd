extends Spatial

export(PackedScene) var hangar_internal_scene;
export(NodePath) onready var area = self.get_node(area) as Area;


func _ready() -> void:
	self.add_to_group(Groups.hangar);
	area.connect("body_entered", self, "_on_area_body_entered");
	area.connect("body_exited", self, "_on_area_body_exited");


func _on_area_body_entered(body : Node) -> void:
	if body is Lumen or body is Player:
		print("Can travel to planet");
		# warning-ignore:return_value_discarded
		body.connect("travel", self, "_on_travel_to_planet");
		body.can_travel = true;


func _on_area_body_exited(body : Node) -> void:
	if body is Lumen or body is Player:
		body.disconnect("travel", self, "_on_travel_to_planet");
		body.can_travel = false;


func _on_travel_to_planet() -> void:
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene_to(hangar_internal_scene);
