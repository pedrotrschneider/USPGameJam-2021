class_name EnemyLocator
extends Spatial

var target : Spatial;
var wr : WeakRef;


func _ready() -> void:
	wr = weakref(target);


func _physics_process(_delta: float) -> void:
	if wr.get_ref():
		self.look_at(target.transform.origin, Vector3.UP);
	else:
		self.hide();
