class_name HangarLocator
extends Spatial

var hangar : Spatial;


func _physics_process(_delta: float) -> void:
	if hangar:
		self.look_at(hangar.transform.origin, Vector3.UP);
