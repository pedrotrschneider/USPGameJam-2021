class_name Lumen
extends KinematicBody

signal travel();

const LUMEN_CUSTOMIZATION_DATA_PATH = "res://resources/runtime_data/LumenCustomizationData.tres";
const LUMEN_DATA_FIELDS = [
	"current_top_weapon",
	"current_side_weapon_front",
	"current_side_weapon_mid",
	"current_side_weapon_back",
	"current_propulsor"
];

export(PackedScene) var hangar_locator_scene;
export(PackedScene) var enemy_locator_scene;
export(NodePath) onready var player_camera = self.get_node(player_camera) as PlayerCamera;
export(NodePath) onready var visual = self.get_node(visual) as Spatial;
export(NodePath) onready var enemy_locators = self.get_node(enemy_locators) as Spatial;
export(NodePath) onready var enemy_locator_timer = self.get_node(enemy_locator_timer) as Timer;
export(NodePath) onready var hangar_locators = self.get_node(hangar_locators) as Spatial;
export(Array, NodePath) var visual_paths = [];
export var walk_speed : float = 10.0;
export var jog_speed : float = 15.0;
export var run_speed : float = 500.0;
export var jog_threshold : float = 0.5;
export var rotation_speed : float = 1.0;
export var movement_acceleration : float = 6;
export var angular_acceleration : float = 7;
export var camera_vertical_offset : float = 50.0;

var lumen_customization_data : LumenCustomizationData = load(LUMEN_CUSTOMIZATION_DATA_PATH) as LumenCustomizationData;
var accelerate : float = 0.0;
var turn : float = 0.0;
var upward : float = 0.0;
var movement_speed : float = 0.0;
var lumen_rotation : float = 0.0;
var move_direction : Vector3 = Vector3.FORWARD;
var movement_velocity : Vector3 = Vector3.ZERO;
var can_travel : bool = false;


func _ready() -> void:
	enemy_locator_timer.connect("timeout", self, "locate_enemies");
	locate_enemies();
	for i in visual_paths.size():
		var v : Spatial = self.get_node(visual_paths[i]);
		v.get_child(lumen_customization_data[LUMEN_DATA_FIELDS[i]]).show();
	
	yield(get_tree().create_timer(1), "timeout");
	for h in self.get_tree().get_nodes_in_group(Groups.hangar):
		var nhg : HangarLocator = hangar_locator_scene.instance() as HangarLocator;
		nhg.hangar = h;
		hangar_locators.add_child(nhg);


func _physics_process(delta: float) -> void:
	if can_travel:
		if Input.is_action_pressed("lumen_travel"):
			self.emit_signal("travel");
	
	if Input.is_action_pressed("lumen_left") or \
	   Input.is_action_pressed("lumen_right"):
		turn = Input.get_action_strength("lumen_left") \
			- Input.get_action_strength("lumen_right");
		
		lumen_rotation += turn * rotation_speed * delta;
		lumen_rotation = lumen_rotation - (2 * PI * sign(lumen_rotation)) \
			if abs(lumen_rotation) >= 2 * PI else lumen_rotation;
	
	var vertical_rotation : float = player_camera.get_vertical(). \
		global_transform.basis.get_euler().x - camera_vertical_offset;
	
	if Input.is_action_pressed("lumen_accelerate") or \
	   Input.is_action_pressed("lumen_reverse"):
		
		accelerate = Input.get_action_strength("lumen_accelerate") \
			- Input.get_action_strength("lumen_reverse");
		
		if Input.is_action_pressed("lumen_turbo"):
			movement_speed = run_speed;
		else:
			if move_direction.length_squared() >= jog_threshold:
				movement_speed = jog_speed;
			else:
				movement_speed = walk_speed;
	else:
		movement_speed = 0.0;
	
	move_direction = Vector3.FORWARD.rotated(Vector3.UP, lumen_rotation);
	move_direction = move_direction.rotated(visual.global_transform.basis.x, \
		vertical_rotation);
	
	movement_velocity = lerp(movement_velocity, move_direction \
		* accelerate * movement_speed, movement_acceleration * delta);
	
	# warning-ignore:return_value_discarded
	self.move_and_slide(movement_velocity, Vector3.UP);
	
	visual.look_at(self.global_transform.origin + move_direction, Vector3.UP);


func locate_enemies() -> void:
	enemy_locator_timer.wait_time = 5;
	for c in enemy_locators.get_children():
		enemy_locators.remove_child(c);
		c.queue_free();
	var enemies : Array = self.get_tree().get_nodes_in_group(Groups.space_enemy_encounter);
	for enemy in enemies:
		var nel : EnemyLocator = enemy_locator_scene.instance();
		nel.target = enemy;
		enemy_locators.add_child(nel);
		nel.look_at(nel.target.transform.origin, Vector3.UP);
