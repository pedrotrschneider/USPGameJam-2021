tool
extends Spatial

export(PackedScene) var planet_level_scene;
export var collision_limit_radius : float = 200.0 setget set_collision_limit_radius;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	$Area.connect("body_entered", self, "_on_area_body_entered");
	# warning-ignore:return_value_discarded
	$Area.connect("body_exited", self, "_on_area_body_exited");


func _on_area_body_entered(body : Node) -> void:
	if body is Lumen:
		print("Can travel to planet");
		# warning-ignore:return_value_discarded
		body.connect("travel", self, "_on_travel_to_planet");
		body.can_travel = true;


func _on_area_body_exited(body : Node ) -> void:
	if body is Lumen:
		print("Can't travel to planet anymore");
		body.disconnect("travel", self, "_on_travel_to_planet");
		body.can_travel = false;


func _on_travel_to_planet() -> void:
	print("Changed planet");
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene_to(planet_level_scene);


func set_collision_limit_radius(val : float) -> void:
	collision_limit_radius = val;
	if not self.is_inside_tree(): return;
	var new_shape : SphereShape = SphereShape.new();
	new_shape.radius = val;
	$CollisionShape.shape = new_shape;
	var new_area_shape : SphereShape = SphereShape.new();
	new_area_shape.radius = 1.5 * val;
	$Area/CollisionShape.shape = new_area_shape;
