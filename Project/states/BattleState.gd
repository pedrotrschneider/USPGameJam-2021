extends Node

enum {
	Idle,
	EnemyChoosingAction,
	EnemyChoosingTarget,
	PlayerChoosingAction,
	PlayerChoosingTarget
}

var state : int = Idle;
var active_fighter : Fighter = null;
